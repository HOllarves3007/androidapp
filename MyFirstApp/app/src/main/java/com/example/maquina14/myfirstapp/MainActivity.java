package com.example.maquina14.myfirstapp;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.support.v7.widget.ShareActionProvider;
import android.widget.Toast;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;


public class MainActivity extends ActionBarActivity implements View.OnClickListener, AdapterView.OnItemClickListener  {

    Button mainButton;
    TextView mainTextView;
    EditText editText;
    ListView mainListView;
    JSONAdapter mJsonAdapter;
    TextView logoText;
    ArrayList mNameList = new ArrayList();
    ShareActionProvider mShareActionProvider;
    private static final String PREFS = "prefs";
    private static final String PREF_NAME = "name";
    SharedPreferences mSharedPreferences;
    private static final String QUERY_URL = "http://openlibrary.org/search.json?q=";
    ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mainTextView = (TextView) findViewById(R.id.textview);
        mainButton = (Button) findViewById(R.id.main_button);
        mainButton.setOnClickListener(this);
        editText = (EditText) findViewById(R.id.main_edit_text);
        mainListView = (ListView) findViewById(R.id.main_listview);
        logoText = (TextView) findViewById(R.id.logo_text);
        mainListView.setOnItemClickListener(this);
        displayWelcome();
        mJsonAdapter = new JSONAdapter(this, getLayoutInflater());
        mainListView.setAdapter(mJsonAdapter);
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage("Searching for your book!");
        mProgressDialog.setCancelable(false);

        Typeface font = Typeface.createFromAsset(getAssets(), "Dancing Script.ttf");

        mainTextView.setTypeface(font);
        mainButton.setTypeface(font);
        editText.setTypeface(font);
        logoText.setTypeface(font);

    }

    public void displayWelcome() {
        mSharedPreferences = getSharedPreferences(PREFS, MODE_PRIVATE);
        String name = mSharedPreferences.getString(PREF_NAME, "");
        if (name.length() > 0) {
            Toast.makeText(this, "Welcome back, " + name + "!", Toast.LENGTH_LONG).show();
        } else {
            AlertDialog.Builder alert = new AlertDialog.Builder(this);
            alert.setTitle("Hello!");
            alert.setMessage("What is your name?");
            final EditText input = new EditText(this);
            alert.setView(input);

            alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    String inputName = input.getText().toString();
                    SharedPreferences.Editor e = mSharedPreferences.edit();
                    e.putString(PREF_NAME, inputName);
                    e.commit();
                    Toast.makeText(getApplicationContext(), "Welcome " + inputName + "!", Toast.LENGTH_LONG).show();
                }
            });
            alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                }
            });
            alert.show();
        }
    }

    protected void queryBooks(String searchString){
        String urlString = "";
        try{
            urlString = URLEncoder.encode(searchString, "UTF-8");
            Log.d("UrlString: ", urlString);
            Log.d("QUERY", QUERY_URL);
        } catch(UnsupportedEncodingException e) {
            e.printStackTrace();
            Toast.makeText(this, "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
        }
        AsyncHttpClient client = new AsyncHttpClient();
        mProgressDialog.show();
        client.get(QUERY_URL + urlString,
                new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(JSONObject jsonObject) {
                        mProgressDialog.dismiss();
                        Toast.makeText(getApplicationContext(), "Success!", Toast.LENGTH_LONG).show();
                        mJsonAdapter.updateData(jsonObject.optJSONArray("docs"));
                    }
                    @Override
                    public void onFailure(int statusCode, Throwable throwable, JSONObject error) {
                        mProgressDialog.dismiss();
                        Toast.makeText(getApplicationContext(), "Error: " + statusCode + " " + throwable.getMessage(), Toast.LENGTH_LONG).show();
                        Log.d("Ponsha Libros", statusCode + " " + throwable.getMessage());
                    }
                });
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        MenuItem shareItem = menu.findItem(R.id.menu_item_share);
        if (shareItem != null) {
            mShareActionProvider = (ShareActionProvider) MenuItemCompat.getActionProvider(shareItem);
        }
        return true;
    }

    @Override
    public void onClick(View v) {
        queryBooks(editText.getText().toString());
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        JSONObject jsonObject = (JSONObject) mJsonAdapter.getItem(position);
        String coverID = jsonObject.optString("cover_i");
        String title_book = jsonObject.optString("title");
        String author_name = jsonObject.optString("author_name");
        String year_published = jsonObject.optString("first_publish_year");
        String descripton = jsonObject.optString("description");
        Intent detailIntent = new Intent(this, DetailActivity.class);
        detailIntent.putExtra("title_book", title_book);
        detailIntent.putExtra("coverID", coverID);
        detailIntent.putExtra("author_name", author_name);
        detailIntent.putExtra("year_published", year_published);
        startActivity(detailIntent);
        Log.d("Full object", jsonObject.toString());
        Log.d("Description", descripton);

    }

}

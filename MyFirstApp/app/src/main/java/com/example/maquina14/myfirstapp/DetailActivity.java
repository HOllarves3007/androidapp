package com.example.maquina14.myfirstapp;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.support.v7.widget.ShareActionProvider;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

/**
 * Created by Maquina14 on 27/07/2015.
 */
public class DetailActivity extends ActionBarActivity {

    private static final String IMAGE_URL_BASE = "http://covers.openlibrary.org/b/id/";
    String mImageUrl;
    ShareActionProvider mShareActionProvider;
    Typeface font = Typeface.createFromAsset(getAssets(), "Dancing Script.ttf");


    protected void onCreate(Bundle savedInstanceState){

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ImageView imageView  = (ImageView) findViewById(R.id.img_cover);
        TextView authorTextView = (TextView) findViewById(R.id.author_name);
        TextView titleTextView = (TextView) findViewById(R.id.title_book);
        TextView yearTextView = (TextView) findViewById(R.id.published_year);

        /*authorTextView.setTypeface(font);
        titleTextView.setTypeface(font);
        yearTextView.setTypeface(font);*/

        String coverID = this.getIntent().getExtras().getString("coverID");
        if(coverID.length() > 0){
            mImageUrl = IMAGE_URL_BASE + coverID + "-L.jpg";

            Picasso.with(this).load(mImageUrl).placeholder(R.drawable.img_books_large).into(imageView);
        }

        String authorView = this.getIntent().getExtras().getString("author_name");
        String titleView = this.getIntent().getExtras().getString("title_book");
        String yearView = this.getIntent().getExtras().getString("year_published");

        authorTextView.setText(authorView);
        titleTextView.setText(titleView);
        yearTextView.setText(yearView);
    }
    private void setShareIntent(){
        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.setType("text/plain");
        shareIntent.putExtra(Intent.EXTRA_SUBJECT, "Te recomiendo este libro!");
        shareIntent.putExtra(Intent.EXTRA_TEXT, mImageUrl);

        mShareActionProvider.setShareIntent(shareIntent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        MenuItem shareItem = menu.findItem(R.id.menu_item_share);
        if (shareItem != null) {
            mShareActionProvider
                    = (ShareActionProvider) MenuItemCompat.getActionProvider(shareItem);
        }
        setShareIntent();
        return true;
    }
}
